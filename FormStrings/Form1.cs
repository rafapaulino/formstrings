﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormStrings
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = textBox1.Text.ToUpper();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = textBox1.Text.ToLower();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            label1.Text = textBox1.Text.Substring(0,10);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int posicao = textBox1.Text.IndexOf("Rafael");
            label1.Text = "Posição: " + posicao;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.StartsWith("Rafael"))
            {
                MessageBox.Show("Sim, Inicia com Rafael");
            }
            else
            {
                MessageBox.Show("Não, Inicia com Rafael");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.EndsWith("Lindão"))
            {
                MessageBox.Show("Sim, Finaliza com Lindão");
            }
            else
            {
                MessageBox.Show("Não, Finaliza com Lindão");
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            label1.Text = textBox1.Text.PadLeft(30, '*');
        }

        private void button8_Click(object sender, EventArgs e)
        {
            label1.Text = textBox1.Text.PadRight(30, '*');
        }

        private void button9_Click(object sender, EventArgs e)
        {
            label1.Text = textBox1.Text.Trim();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            label1.Text = textBox1.Text.TrimStart();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            label1.Text = textBox1.Text.TrimEnd();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            string[] arrayTexto = textBox1.Text.Split(',');
            int tamanhoArray = arrayTexto.Length;
            MessageBox.Show("Tamanho = " + tamanhoArray.ToString());

            for(int i = 0; i < tamanhoArray; i++)
            {
                string mensagem = "Conteúdo da Posição: " + i + " = " + arrayTexto[i];
                MessageBox.Show(mensagem);
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            string[] nomesArray = {"Juliana","Mariana","Paula","Natália","Amanda"};
            string nomeString = String.Join(" - ",nomesArray);
            label1.Text = nomeString;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            MessageBox.Show(String.Format("O Conteúdo do TextBox é: {0}",textBox1.Text));
        }

        private void button15_Click(object sender, EventArgs e)
        {
            MessageBox.Show(String.Format("Eu tenho {0} camisetas, {1} shorts, {2} tênis!", 3,4,7));
        }

        private void button16_Click(object sender, EventArgs e)
        {
            //indica que o parametro seja obrigatoriamente uma string {0:S}
            MessageBox.Show(String.Format("O Conteúdo é: {0:S}", "Hahaha"));

            //indica que o parametro seja obrigatoriamente um inteiro {0:D}
            MessageBox.Show(String.Format("O Conteúdo é: {0:D}", 10));

            //indica que o parametro seja obrigatoriamente um decimal {0:F}
            MessageBox.Show(String.Format("O Conteúdo é: {0:F}", 10.5));

            //indica que o parametro seja obrigatoriamente um decimal variavel {0:N}
            MessageBox.Show(String.Format("O Conteúdo é: {0:N}", 10.5));

            //indica que o parametro seja obrigatoriamente um decimal variavel {0:N}
            MessageBox.Show(String.Format("O Conteúdo é: {0,30:S}", "20"));
        }

        private void button17_Click(object sender, EventArgs e)
        {
            int numero = 20;
            string numeroFormatado = String.Format("O Conteúdo é: {0,8:D}", numero);
            MessageBox.Show(numeroFormatado);

            numeroFormatado = String.Format("O Conteúdo é: {0,-8:D}FIM", numero);
            MessageBox.Show(numeroFormatado);

            numeroFormatado = String.Format("Número complementando com Zero: {0:D5}FIM", numero);
            MessageBox.Show(numeroFormatado);

            numeroFormatado = String.Format("Número complementando com Zero e Espaços em Branco: {0,8:D5}FIM", numero);
            MessageBox.Show(numeroFormatado);

            numeroFormatado = String.Format("Número complementando com Zero e Espaços em Branco[2]: {0,-8:D5}FIM", numero);
            MessageBox.Show(numeroFormatado);
        }

        private void button18_Click(object sender, EventArgs e)
        {
            string variavelDecimal = String.Format("Decimais {0:F}", 10.3);
            MessageBox.Show(variavelDecimal);

            //4 casas decimais
            variavelDecimal = String.Format("Decimais {0:F4}", 10.3);
            MessageBox.Show(variavelDecimal);

            //10 casas decimais
            variavelDecimal = String.Format("Decimais {0:F10}", 10.3);
            MessageBox.Show(variavelDecimal);

            variavelDecimal = String.Format("Decimais {0:F}", 10007.356);
            MessageBox.Show(variavelDecimal);

            variavelDecimal = String.Format("Decimais com milhar {0:N}", 10007.356);
            MessageBox.Show(variavelDecimal);

            variavelDecimal = String.Format("Decimais com milhar {0:N2}", 10007.356);
            MessageBox.Show(variavelDecimal);

            variavelDecimal = String.Format("Decimais com Moeda {0:C}", 10007.356);
            MessageBox.Show(variavelDecimal);

            variavelDecimal = String.Format("Percentual {0:P}", 2.3);
            MessageBox.Show(variavelDecimal);

            variavelDecimal = String.Format("Percentual {0:P6}", 2.3);
            MessageBox.Show(variavelDecimal);
        }

        private void button19_Click(object sender, EventArgs e)
        {
            DateTime dataHoraAtual = System.DateTime.Now;
            String data = String.Format("Data Atual Completa: {0:D}", dataHoraAtual);
            MessageBox.Show(data);

            data = String.Format("Data Simples: {0:d}", dataHoraAtual);
            MessageBox.Show(data);

            String hora = String.Format("Hora Completa: {0:T}", dataHoraAtual);
            MessageBox.Show(hora);

            hora = String.Format("Hora Simples: {0:t}", dataHoraAtual);
            MessageBox.Show(hora);

            String DataEHora = String.Format("Data e Hora Completa: {0:F}", dataHoraAtual);
            MessageBox.Show(DataEHora);

            DataEHora = String.Format("Data e Hora Simples: {0:f}", dataHoraAtual);
            MessageBox.Show(DataEHora);

            DataEHora = String.Format("Data Simples e Hora Simples: {0:G}", dataHoraAtual);
            MessageBox.Show(DataEHora);

            DataEHora = String.Format("Data Simples e Hora Simples: {0:g}", dataHoraAtual);
            MessageBox.Show(DataEHora);

            DataEHora = String.Format("Dia e Mês: {0:M}", dataHoraAtual);
            MessageBox.Show(DataEHora);

            DataEHora = String.Format("Mês e Ano: {0:Y}", dataHoraAtual);
            MessageBox.Show(DataEHora);

            DataEHora = String.Format("Data Personalizada: {0:dd/MM/yyy hh:mm:ss}", dataHoraAtual);
            MessageBox.Show(DataEHora);
        }
    }
}
